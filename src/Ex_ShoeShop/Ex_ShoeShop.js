import React, { Component } from 'react'
import { dataShoe } from './dataShoe'
import ListShoe from './ListShoe';
import DetailShoe from './DetailShoe';
import Cart from './Cart';


export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
  };
  handleChangeDetail = (value) => {
    this.setState({
      detail: value,
    });
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];

    let index = this.state.cart.findIndex((item) => {
      return item.id === shoe.id;
    });

    if (index === -1) {
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }
    this.setState({ cart: cloneCart });
  };

  // tạo function add số vào cart

  handleChangeQty = (data, value) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id === data;
    });
    if (cloneCart[index].number === 1 && value === -1) {
      cloneCart.splice(index, 1);
    } else {
      cloneCart[index].number += value;
    }
    this.setState({ cart: cloneCart });
  };

  render() {
    return (
      <div className="container">
        <Cart cart={this.state.cart} handleChangeQty={this.handleChangeQty} />
        <ListShoe
          shoeArr={this.state.shoeArr}
          handleChangeDetail={this.handleChangeDetail}
          handleAddToCart={this.handleAddToCart}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}



