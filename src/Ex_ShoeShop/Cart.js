import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.number}</td>
          <td>
            <button
              className="btn btn-success mr-1"
              onClick={() => {
                this.props.handleChangeQty(item.id, +1);
              }}> Hốt </button>

            <button className="btn btn-info mx-2">{item.number}</button>

            <button
              className="btn btn-danger ml-1"
              onClick={() => {
                this.props.handleChangeQty(item.id, -1);
              }}> Bỏ </button>

          </td>
          <td>
            <img style={{ width: "80px" }} src={item.image} alt="true" />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Tên sản phẩm</th>
            <th>Giá</th>
            <th>Số lượng</th>
            <th>Hình ảnh</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
