import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let {image, name} = this.props.data;
    return (
      <div className="col-3">
        <div className="card text-left text-light h-100">
          <img
            style={{ height: "80%", objectFit: "cover" }}
            className="card-img-top "
            src={image}
            alt
          />
          <div className="card-body bg-secondary h-100">
            <h4 className="card-title text-center">
              {name}
            </h4>

            <button onClick={()=>{this.props.handleAddToCart(this.props.data)}} className="btn btn-success mr-5">Hốt ngay !!</button>
            
            <button onClick={()=>{this.props.handleViewDetail(this.props.data)}} className="btn btn-warning">Chi tiết</button>
          </div>
        </div>
      </div>
    );
  }
}
